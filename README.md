# Установка
После клонирования проекта, выполнить
```shell script
sudo docker-compose build
```

Затем
```shell script
sudo docker-compose up
```

Далее, следует прогнать миграцию
```shell script
sudo docker exec bookmarkservice_php-fpm_1 php bin/console doctrine:migrations:migrate
```


Урл проекта: http://bookmarks.com:9090/

