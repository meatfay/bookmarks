<?php

namespace App\Repository;

use App\Entity\Bookmark;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class BookmarkRepository
 * @method Bookmark|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bookmark|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bookmark[] findAll()
 * @method Bookmark[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookmarkRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Bookmark::class);
    }

    /**
     * Create query with criteria
     *
     * @param Criteria $criteria
     * @return \Doctrine\ORM\QueryBuilder
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function createSortableQuery(Criteria $criteria)
    {
        return $this->createQueryBuilder('b')
            ->addCriteria($criteria);
    }

    /**
     * Save bookmark object
     *
     * @param Bookmark $bookmark
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($bookmark)
    {
        $this->getEntityManager()->persist($bookmark);
        $this->getEntityManager()->flush($bookmark);
    }

    /**
     * Remove bookmark entity
     *
     * @param object $bookmark
     * @param bool $flush
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove($bookmark)
    {
        $this->getEntityManager()->remove($bookmark);
        $this->getEntityManager()->flush($bookmark);
    }
}