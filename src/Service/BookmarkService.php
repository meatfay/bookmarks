<?php

namespace App\Service;

use App\Entity\Bookmark;
use App\Repository\BookmarkRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use League\Uri\Parser;

class BookmarkService
{
    /**
     * @var BookmarkRepository
     */
    private $bookmarkRepository;

    /**
     * @var Client
     */
    private $elasticsearchClient;

    public function __construct(
        Client $elasticsearchClient,
        BookmarkRepository $bookmarkRepository)
    {
        $this->bookmarkRepository = $bookmarkRepository;
        $this->elasticsearchClient = $elasticsearchClient;
    }

    /**
     * Find Bookmark by id
     *
     * @param integer $id
     * @return Bookmark
     */
    public function findBookmarkById($id)
    {
        return $this->bookmarkRepository->find(intval($id));
    }

    /**
     * Find all Bookmarks without sorting
     *
     * @return Bookmark[]
     */
    public function findAllBookmarks(): array
    {
        return $this->bookmarkRepository->findAll();
    }

    /**
     * Find bookmark by url
     *
     * @param $url
     * @return Bookmark|null
     */
    public function findBookmarksByUrl($url): ?Bookmark
    {
        return $this->bookmarkRepository->findOneBy(['url' => $url]);
    }

    public function searchBookmarks($query)
    {
        $response = $this->elasticsearchClient->search([
            'index' => 'bookmark',
            'body' => [
                'query' => [
                    'multi_match' => [
                        'query' => $query,
                        'type' => 'phrase_prefix',
                        'fields' => ['title', 'url', 'description', 'keywords']
                    ]
                ]
            ]
        ]);


        $idList = [];

        if (!empty($response['hits']) && !empty($response['hits']['hits'])) {
            foreach ($response['hits']['hits'] as $hit) {
                $idList[] = intval($hit['_source']['id']);
            }
        }

        return $idList;
    }

    /**
     * Build query builder for pagination
     *
     * @param $sortField
     * @param $sortDirection
     * @param array $ids
     * @return \Doctrine\ORM\QueryBuilder
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getQueryWithCriteria($sortField, $sortDirection, $ids = []): QueryBuilder
    {
        if (!in_array($sortField, ['url', 'favicon', 'title', 'metaDescription', 'metaKeywords', 'updatedAt'])) {
            $sortField = 'updatedAt';
        }

        if (!in_array($sortDirection, ['asc', 'desc'])) {
            $sortDirection = 'asc';
        }


        $criteria = Criteria::create()
            ->orderBy([$sortField => $sortDirection]);

        if (!empty($ids)) {
            $criteria->andWhere(Criteria::expr()->in('id', $ids));
        }

        return $this->bookmarkRepository->createSortableQuery($criteria);
    }

    /**
     * Remove bookmark by password
     *
     * @param Bookmark $bookmark
     * @param $password
     * @return bool
     */
    public function removeBookmark(Bookmark $bookmark, $password)
    {
        if (!password_verify($password, $bookmark->getPassword())) {
            return false;
        }

        try {
            $this->bookmarkRepository->remove($bookmark);
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * Create and save new Bookmark
     *
     * @param array $parsedUrl
     * @param array $data
     * @param string $password
     * @return Bookmark
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createBookmark($parsedUrl, $data, $password = '')
    {
        $favicon = 'Favicon не указан';

        if (!empty($data['favicon'])) {
            $urlParser = new Parser();

            $parsedFavicon = $urlParser($data['favicon']);

            if (is_null($parsedFavicon['scheme']) || is_null($parsedFavicon['host'])) {
                $favicon = sprintf('%s://%s%s', $parsedUrl['scheme'], $parsedUrl['host'], $data['favicon']);
            } else {
                $favicon = $data['favicon'];
            }
        }

        $bookmark = new Bookmark();

        $bookmark->setUrl($data['url']);
        $bookmark->setTitle($data['title'] ?? 'Заголовок не указан');
        $bookmark->setMetaDescription($data['description'] ?? 'Описание не указано');
        $bookmark->setMetaKeywords($data['keywords'] ?? 'Ключевые слова не указаны');
        $bookmark->setFavicon($favicon);

        if ($password) {
            $password = password_hash($password, CRYPT_BLOWFISH);

            if ($password) {
                $bookmark->setPassword($password);
            }
        }

        $this->bookmarkRepository->save($bookmark);

        $this->elasticsearchClient->index([
            'index' => 'bookmark',
            'type' => 'bookmarks',
            'body' => [
                'id' => $bookmark->getId(),
                'title' => $bookmark->getTitle(),
                'url' => $bookmark->getUrl(),
                'description' => $bookmark->getMetaDescription(),
                'keywords' => $bookmark->getMetaKeywords(),
            ],
        ]);

        return $bookmark;
    }
}