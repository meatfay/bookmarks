<?php

namespace App\Service;

use App\Entity\Bookmark;

class ExcelBookmarksService
{
    public function generate(array $bookmarks)
    {
        $file = fopen('php://memory', 'w');

        $headers = [
            'Дата обновления',
            'URL',
            'Заголовок',
            'Favicon',
            'Описание',
            'Ключевые слова',
        ];

        fputcsv($file, $headers, ',');

        /** @var Bookmark $bookmark */
        foreach ($bookmarks as $bookmark) {
            $line = [
                $bookmark->getUpdatedAt() ? $bookmark->getUpdatedAt()->format('Y-m-d H:i:s') : 'Не обновлялось',
                $bookmark->getUrl(),
                $bookmark->getTitle(),
                $bookmark->getFavicon(),
                $bookmark->getMetaDescription(),
                $bookmark->getMetaKeywords(),
            ];

            fputcsv($file, $line, ',');
        }

        fseek($file, 0);
        return $file;
    }
}