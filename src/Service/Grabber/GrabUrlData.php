<?php

namespace App\Service;

use App\Service\Grabber\Exception\GrabberException;
use DOMElement;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;

class GrabUrlData
{
    /**
     * @param string $url
     * @return array
     */
    public function getUrlData(string $url): array
    {
        $httpClient = HttpClient::create();

        try {
            $response = $httpClient->request('GET', $url);

            $data = $this->parseDom($response->getContent());
            $data['url'] = $url;

            return $data;
        } catch (\Throwable $ex) {
            var_dump($ex->getMessage());
            exit();
            return [];
        }
    }

    /**
     * Parse DOM content
     * Return parsed data in array
     *
     * @param string $content
     * @return array
     * @throws GrabberException
     */
    private function parseDom(string $content): array
    {
        $dom = new Crawler($content);

        $headNode = $this->getHeadData($dom);

        if (!$headNode) {
            throw new GrabberException("Cannot find head node in content");
        }

        $data = [];

        $titleElement = $headNode->getElementsByTagName('title');

        if ($titleElement->length > 0) {
            $data['title'] = $titleElement[0]->textContent;
        }

        $titleElement = $headNode->getElementsByTagName('meta');

        foreach ($titleElement as $metaElement) {
            $data = array_merge($data, $this->parseMetaData($metaElement));
        }

        $linkNodeList = $headNode->getElementsByTagName('link');

        /** @var DOMElement $link */
        foreach ($linkNodeList as $link) {
            if (strpos($link->getAttribute('rel'), 'icon')) {
                $data['favicon'] = $link->getAttribute('href');
            }
        }

        return $data;
    }

    /**
     * Get head tag node
     *
     * @param Crawler $dom
     * @return DOMElement|null
     */
    private function getHeadData(Crawler $dom): ?DOMElement
    {
        foreach ($dom as $domElement) {

            /** @var DOMElement $node */
            foreach ($domElement->childNodes as $node) {
                if ($node instanceof DOMElement && $node->tagName === 'head') {
                    return $node;
                }
            }
        }

        return null;
    }

    /**
     * Parse meta tags list and take required data
     *
     * @param DOMElement $node
     * @return array
     */
    private function parseMetaData(DOMElement $node): array
    {
        if (in_array($node->getAttribute('name'), ['description', 'keywords'])) {
            return [$node->getAttribute('name') => strip_tags($node->getAttribute('content'))];
        }

        return [];
    }
}