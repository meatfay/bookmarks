<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class SortExtension extends AbstractExtension
{

    public function getFunctions()
    {
        return [
            new TwigFunction('sortLink', [$this, 'sortView'])
        ];
    }

    public function sortView($fieldTitle, $fieldName, $currentFieldName, $sortType)
    {
        $arrow = '';

        if ($fieldName == $currentFieldName && $sortType != 'asc') {
            $arrow = '<i class="fa fa-arrow-up" aria-hidden="true"></i>';
        }

        return sprintf('<a href="/?sortField=%s&sortDirection=%s">%s%s</a>', $fieldName, $sortType != 'asc' && $fieldName == $currentFieldName ? 'asc' : 'desc', $fieldTitle, $arrow);
    }
}