<?php

namespace App\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class AddBookmarkForm extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('url', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Поле не должно быть пустым'
                    ]),
                    new Callback(function ($value, ExecutionContextInterface $context) {
                        if (empty($this->parseUrl($value))) {
                            $context->buildViolation('Неверно указан адрес сайта')
                                ->atPath('url')
                                ->addViolation();
                        }
                    }),
                    new Callback(function ($value, ExecutionContextInterface $context) {
                        if (empty($this->parseUrl($value))) {
                            return false;
                        }

                        $parsedUrl = parse_url($value);

                        if (empty($parsedUrl['scheme'])) {
                            $value = 'http://' . $value;
                        }

                        $httpClient = HttpClient::create();

                        try {
                            $response = $httpClient->request('GET', $value);

                            if ($response->getStatusCode() !== 200) {
                                $context->buildViolation('Сервер по адресу {{ url }} не отвечает')
                                    ->setParameter('{{ url }}', $value)
                                    ->atPath('url')
                                    ->addViolation();
                            }
                        } catch (TransportExceptionInterface $e) {
                            $context->buildViolation('Сервер по адресу {{ url }} не отвечает')
                                ->setParameter('{{ url }}', $value)
                                ->atPath('url')
                                ->addViolation();
                        }
                    }),
                ],
                'attr' => [
                    'placeholder' => 'http://bookmark.com...'
                ],
            ])
            ->add('password', PasswordType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Пароль для удаления'
                ],
                'label' => 'Пароль',
            ])
            ->add('add', SubmitType::class, [
                'label' => 'Добавить',
            ]);
    }

    /**
     * Parse url to validate
     * Returns empty array if url is not valid
     *
     * @param $value
     * @return array
     */
    private function parseUrl($value): array
    {
        preg_match('/^((http|https):\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$/', $value, $match);

        return $match;
    }
}