<?php

namespace App\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class RemoveBookmarkForm extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('password', PasswordType::class, [
                'label' => 'Пароль',
                'attr' => [
                    'placeholder' => 'Пароль для удаления закладки',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Поле не должно быть пустым'
                    ]),
                ]
            ])
            ->add('remove', SubmitType::class, [
                'label' => 'Удалить',
            ]);
    }
}