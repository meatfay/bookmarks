<?php

namespace App\Factory;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

class ESFactory
{
    /**
     * @var Client null
     */
    private static $client = null;

    public static function getClient()
    {
        if (is_null(self::$client)) {
            self::$client = ClientBuilder::create()->setHosts(['es:9200'])->build();
        }

        return self::$client;
    }
}