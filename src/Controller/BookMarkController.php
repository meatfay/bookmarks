<?php

namespace App\Controller;

use App\Forms\AddBookmarkForm;
use App\Forms\Entity\AddBookmark;
use App\Forms\Entity\RemoveBookmark;
use App\Forms\RemoveBookmarkForm;
use App\Service\BookmarkService;
use App\Service\ExcelBookmarksService;
use App\Service\GrabUrlData;
use Knp\Component\Pager\PaginatorInterface;
use League\Uri\Parser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

class BookMarkController extends AbstractController
{
    /**
     * @var BookmarkService
     */
    private $bookmarkService;

    /**
     * @var ExcelBookmarksService
     */
    private $excelBookmarksService;

    public function __construct(
        BookmarkService $bookmarkService,
        ExcelBookmarksService $excelBookmarksService
    )
    {
        $this->bookmarkService = $bookmarkService;
        $this->excelBookmarksService = $excelBookmarksService;
    }

    /**
     * @Route("/", methods="GET")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function getBookmarksList(Request $request, PaginatorInterface $paginator): Response
    {
        $sortField = $request->get('sortField');
        $sortDirection = $request->get('sortDirection');

        $qb = $this->bookmarkService->getQueryWithCriteria($sortField, $sortDirection);

        $pagination = $paginator->paginate(
            $qb,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('bookmark/bookmarkList.html.twig', [
            'pagination' => $pagination,
            'sortField' => $sortField,
            'sortDirection' => $sortDirection,
            'query' => '',
        ]);
    }

    /**
     * @Route("/bookmark/{id}", methods="GET")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function getBookmark(Request $request, $id): Response
    {
        $bookmark = $this->bookmarkService->findBookmarkById($id);

        if (!$bookmark) {
            throw $this->createNotFoundException('Закладка не найдена');
        }

        return $this->render('bookmark/bookmark.html.twig', [
            'bookmark' => $bookmark,
        ]);
    }

    /**
     * @Route("/bookmark/{id}/remove", methods={"GET", "POST"})
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function removeBookmark(Request $request, $id): Response
    {
        $bookmark = $this->bookmarkService->findBookmarkById($id);

        if (!$bookmark || !$bookmark->getPassword()) {
            throw $this->createNotFoundException('Закладка не найдена');
        }

        $removeBookmarkEntity = new RemoveBookmark();

        $form = $this->createForm(RemoveBookmarkForm::class, $removeBookmarkEntity);

        $form->handleRequest($request);

        $errors = [];

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var RemoveBookmark $formData */
            $formData = $form->getData();

            if ($this->bookmarkService->removeBookmark($bookmark, $formData->getPassword())) {
                $this->addFlash('success', sprintf('Закладка %s была успешно удалена', $bookmark->getUrl()));
                return $this->redirect('/');
            }
        }

        return $this->render('bookmark/bookmarkAddForm.html.twig', [
            'form' => $form->createView(),
            'errors' => $errors,
        ]);
    }

    /**
     * @Route("/add", methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     */
    public function addBookmark(Request $request): Response
    {
        $addBookmarkEntity = new AddBookmark();

        $errors = [];

        $form = $this->createForm(AddBookmarkForm::class, $addBookmarkEntity);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var AddBookmark $formData */
            $formData = $form->getData();

            $url = $formData->getUrl();

            $urlParser = new Parser();

            $parsedUrl = $urlParser($url);

            if (empty($parsedUrl['scheme'])) {
                $url = 'http://' . $url;
                $parsedUrl = $urlParser($url);
            }

            $bookmark = $this->bookmarkService->findBookmarksByUrl($url);

            if ($bookmark) {
                return $this->redirect('/bookmark/' . $bookmark->getId());
            }

            $grabber = new GrabUrlData();

            $grabbedData = $grabber->getUrlData($url);

            if (empty($grabbedData)) {
                $errors[] = 'Невозможно получить данные с указанного адреса';
            }

            try {
                $bookmark = $this->bookmarkService->createBookmark($parsedUrl, $grabbedData, $formData->getPassword());
                return $this->redirect('/bookmark/' . $bookmark->getId());
            } catch (\Exception $ex) {
                $errors[] = $ex->getMessage();
                return $this->render('bookmark/bookmarkAddForm.html.twig', [
                    'form' => $form->createView(),
                    'errors' => $errors,
                ]);
            }
        }

        return $this->render('bookmark/bookmarkAddForm.html.twig', [
            'form' => $form->createView(),
            'errors' => $errors,
        ]);
    }

    /**
     * @Route("/export", methods="GET")
     * @param Request $request
     * @return Response
     */
    public function buildExcel(Request $request)
    {
        $bookmarks = $this->bookmarkService->findAllBookmarks();

        $response = new StreamedResponse(function () use ($bookmarks) {
            $outputStream = fopen('php://output', 'wb');

            $file = $this->excelBookmarksService->generate($bookmarks);

            stream_copy_to_stream($file, $outputStream);
        });

        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Content-Disposition', 'attachment; filename="output.xlsx";');

        return $response;
    }

    /**
     * @Route("/search", methods="GET")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function search(Request $request, PaginatorInterface $paginator)
    {
        $query = $request->get('query');
        $sortField = $request->get('sortField');
        $sortDirection = $request->get('sortDirection');

        $bookmarkIds = $this->bookmarkService->searchBookmarks($query);

        $qb = $this->bookmarkService->getQueryWithCriteria($sortField, $sortDirection, $bookmarkIds);

        $pagination = $paginator->paginate(
            $qb,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('bookmark/bookmarkList.html.twig', [
            'pagination' => $pagination,
            'sortField' => $sortField,
            'sortDirection' => $sortDirection,
            'query' => $query,
        ]);
    }
}