<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200407201228 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add password field to bookmarks';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("
            ALTER TABLE `bookmark` ADD COLUMN `password` VARCHAR(255) NULL DEFAULT '' AFTER `meta_keywords`
        ");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("
            ALTER TABLE `bookmark` DROP COLUMN `password`
        ");
    }
}
