<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200407165002 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Create bookmarks table';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("
            CREATE TABLE IF NOT EXISTS bookmark (
                id INTEGER(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
                url VARCHAR(255) NOT NULL,
                favicon VARCHAR(255) NOT NULL,
                title VARCHAR(255) NOT NULL,
                meta_description TEXT,
                meta_keywords TEXT,
                updated_at DATETIME NULL DEFAULT NOW() ON UPDATE NOW(),
                created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
            ) ENGINE InnoDB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
        ");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE bookmark;');
    }
}
